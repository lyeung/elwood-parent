# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The main objective of this project is to learn how to write a simple back-end build-server. This project requires elwood-ui project to provide the web interface.

### Project Home ###

* https://www.hostedredmine.com/projects/elwood-parent

### Project Wiki ###

* https://www.hostedredmine.com/projects/elwood-parent/wiki

### Project Blog ###

* http://tamemymonkeymind.blogspot.com.au/search/label/elwood

### Issue Management ###

* https://www.hostedredmine.com/projects/elwood-parent/issues

### How do I get set up? ###

* Summary of set up
    * Requires JDK 8, Maven 3.3.1+ and Redis 3
* Configuration
    * (TBD)
* Dependencies
    * (TBD)
* Database configuration
    * Start up Redis3 server "redis-server"
* How to run tests
    * Run Maven command "mvn clean install"
* Deployment instructions
    * (TBD)

### Contribution guidelines ###

* Writing tests
    * (TBD)
* Code review
    * (TBD)
* Other guidelines
    * (TBD)

### Who do I talk to? ###

* Repo owner or admin
    * (TBD)
* Other community or team contact
    * (TBD)

